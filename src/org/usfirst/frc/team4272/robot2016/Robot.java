
package org.usfirst.frc.team4272.robot2016;

import edu.wpi.first.wpilibj.IterativeRobot;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {
	private final HwRobot robot = new HwRobot();
	private final HwOI oi = new HwOI();
	private final Control control = new Control();

	private Autonomous auto;
	private Teleop teleop;
	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	public void robotInit() {
	}

	public void autonomousInit() {
		try {
			auto = new Autonomous(robot);
		} catch (Exception e) {}
	}
	public void autonomousPeriodic() {
		try {
			robot.run(auto.run(control));
		} catch (Exception e) {}
	}

	public void teleopInit() {
		try {
			teleop = new Teleop(robot);
		} catch (Exception e) {}
	}

	public void teleopPeriodic() {
		try {
			robot.run(teleop.run(control, oi));
		} catch (Exception e) {}
	}

	public void disabledInit() {
	}

	public void disabledPeriodic() {
	}

	/**
	 * This function is called periodically during test mode
	 */
	public void testPeriodic() {
	}
}
