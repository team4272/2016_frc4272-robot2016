package org.usfirst.frc.team4272.robot2016;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Autonomous {
	private final Timer time = new Timer();
	private final HwRobot robot;

	public Autonomous(HwRobot robot) {
		this.robot = robot;
		time.reset();
		time.start();
	}

	public Control run(Control c) {
		if (robot.armL.get()) {
			c.arm = -0.5;
		} else {
			c.arm = 0;
			robot.armE.reset();
		}

		if (time.get() < 4) {
			c.lDrive1 = c.rDrive1 = -0.4;
		} else {
			c.lDrive1 = c.rDrive1 = 0;
		}
		SmartDashboard.putNumber("autoTime", time.get());

		return c;
	}
}
