package org.usfirst.frc.team4272.robot2016;

import org.usfirst.frc.team4272.robotlib.Xbox360Controller;

import edu.wpi.first.wpilibj.Joystick;

public class HwOI {
	public Joystick lStick = new Joystick(0);
	public Joystick rStick = new Joystick(1);
	public Xbox360Controller xbox = new Xbox360Controller(2);
}
