package org.usfirst.frc.team4272.robot2016;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Timer;
import org.usfirst.frc.team4272.robotlib.Xbox360Controller.Axis;
import org.usfirst.frc.team4272.robotlib.Xbox360Controller.Button;
import org.usfirst.frc.team4272.robotlib.PushButton;

public class Teleop {
	private final HwRobot robot;
	private final double armSoftStop = 95;//295;

	private final PushButton shootButton = new PushButton();
	private final Timer time = new Timer();

	public Teleop(HwRobot robot) {
		this.robot = robot;
		robot.armE.reset();
	}

	private static double jsScale(Joystick j) {
		double y = -j.getY();/* +:forward; -:backward */
		double z = -j.getZ();/* +:more-sensitive; -:less-sensitive */
		return Math.copySign(Math.pow(Math.abs(y), 2.0-z), y);
	}

	private static double bound(double min, double v, double max) {
		return Math.min(Math.max(v, min), max);
	}

	private static double oneIf(boolean b) {
		return b ? 1 : 0;
	}

	public Control run(Control control, HwOI oi) {
		/* Drive */
		control.lDrive1 = jsScale(oi.lStick);
		control.rDrive1 = jsScale(oi.rStick);
		control.lDrive2 = oi.lStick.getTrigger() ? control.lDrive1 : 0;
		control.rDrive2 = oi.rStick.getTrigger() ? control.rDrive1 : 0;
		control.arm = -oi.xbox.getAxis(Axis.LY);

		double fBall = oi.xbox.getAxis(Axis.RTrigger) - oi.xbox.getAxis(Axis.LTrigger);
		double bBall = bound(-1, fBall, 0) + oneIf(oi.xbox.getButton(Button.A)) - oneIf(oi.xbox.getButton(Button.B));

		boolean ballL = robot.ballL.get();
		control.fBall = bound(-oneIf(ballL), fBall < 0 ? 0.75*fBall : fBall, 1);
		control.bBall = bound(-oneIf(ballL), bBall < 0 ? 0.85*bBall : bBall, 1);

		/* I'm eyeballing 10 degrees ≈ 50 clicks */
		if (robot.armE.pidGet() < 50 && !robot.armL.get()) {
			robot.armE.reset();
		}

		if (oi.xbox.getButton(Button.LBumper))
			control.arm = 1-(robot.armE.pidGet()/armSoftStop);
		else if (oi.xbox.getButton(Button.RBumper))
			control.arm = robot.armE.pidGet()/armSoftStop;

		if (shootButton.update(oi.xbox.getButton(Button.X))) {
			time.reset();
			time.start();
		}
		if (oi.xbox.getButton(Button.X)) {
			if (time.get() < 0.5) {
				control.fBall = control.bBall = -0.5;
			} else if (time.get() < 2.5) {
				control.bBall = -0.5;
				control.fBall = 1;
			} else {
				control.bBall = 1;
				control.fBall = 1;
			}
		}

		//if (robot.armE.pidGet() > armSoftStop && control.arm > 0)
		//	control.arm = 0;

		/* Take pictures */
		/*
		if (camButton.update(oi.xbox.getButton(Button.RT))) {
			try {
				ProcessBuilder pb = new ProcessBuilder("/home/lvuser/tweeterbot/bin/snapPhoto");
				pb.redirectErrorStream(true);
				pb.start();
			} catch (Exception e) {};
		}
		*/

		return control;
	}
}
