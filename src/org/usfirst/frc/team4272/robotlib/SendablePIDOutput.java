/**
 * Copyright (c) 2012 Precise Path Robotics, Inc
 * Copyright (c) 2015 Luke Shumaker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Luke Shumaker <lukeshu@sbcglobal.net>
 */
package org.usfirst.frc.team4272.robotlib;

import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * TODO: Write JavaDocs
 */
public class SendablePIDOutput implements PIDOutput {
	PIDOutput o;
	String n;

	public SendablePIDOutput(String name, PIDOutput out) {
		n = name;
		o = out;
	}

	public void pidWrite(double val) {
		o.pidWrite(val);
		SmartDashboard.putNumber(n, val);
	}
}
