/**
 * TODO: copyright
 *
 * @author Luke Shumaker <lukeshu@sbcglobal.net>
 */
package org.usfirst.frc.team4272.robotlib;

/**
 * TODO: Write JavaDocs
 */
public class ToggleButton {
	private boolean prev = false;
	private boolean state = false;
	public boolean update(boolean next) {
		if (next && ! prev) {
			state = !state;
		}
		prev = next;
		return state;
	}
}
